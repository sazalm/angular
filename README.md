# Angular js Tutorial for Beginner#
All Credit goes to [w3schools](http://www.w3schools.com)
This tutorial is specially designed to help you learn AngularJS as quickly and efficiently as possible.

First, you will learn the basics of AngularJS: directives, expressions, filters, modules, and controllers.

Then you will learn everything else you need to know about AngularJS:

Events, DOM, Forms, Input, Validation, Http, and more.



### What is this repository for? ###

* Quick summary
* Version 1.4.8
* [Learn More From w3schools.com](http://www.w3schools.com/angular/default.asp)

### How do I get set up? ###

[Download Angular js from here](http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js)


### Who do I talk to? ###
* Amanullah Aman
* Admin of this project

### What we learn? ###
* Inroduction
* Expression
* Modules
* Directives
* Model
* Controllers
* Scopes
* Filter
* Service
* HTTP
* Tables
* Select
* SQL
* DOM
* Events
* Forms
* Validation
* API
* Includes
* Animation
* Application

### Introduction ###
* How to start angular js??
--> See below code

```
#!html

<!DOCTYPE html>
<html>
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<body>

<div ng-app="">
 
<p>Input something in the input box:</p>
<p>Name: <input type="text" ng-model="name"></p>
<p ng-bind="name"></p>

</div>

</body>
</html>
```